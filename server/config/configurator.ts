import * as nconf from 'nconf';

class Configurator {
    constructor() {
        nconf.argv().env();

        if(nconf.get('WPS_ENV') === undefined || nconf.get('WPS_ENV') === null){
            console.error('FATAL No value set for WPS_ENV variable');
            process.exit(9);
        } else {
            return this;
        }
    }

    getEnvironment() {
        return nconf.get('WPS_ENV')? nconf.get('WPS_ENV').toLowerCase(): nconf.get('WPS_ENV');
    }

    getEnvironmentConfigFile() {
        return `${this.getEnvironment()}-wps-environment-config.json`;
    }
}

export const configurator: any = new Configurator();
