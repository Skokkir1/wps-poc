import { Router } from 'express';
import { tokenApi } from '../api/token.api';
import { searchSimApi } from '../api/searchSim.api';

export const wpsRoute = (router: Router): void => {
    router.post('/token', tokenApi.invoke);
    router.post('/searchSim', searchSimApi.invoke);
};
