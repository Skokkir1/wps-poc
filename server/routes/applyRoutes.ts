import { Router } from 'express';
import { commonRoute } from './commonRoutes';
import { wpsRoute } from './wpsRoutes';

export const applyRoutes = (router: Router) => {
    commonRoute(router);
    wpsRoute(router);
    return router;
};
