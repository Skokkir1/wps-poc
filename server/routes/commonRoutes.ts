import { Router } from 'express';
import { healthCheck } from '../api/healthCheck.api';

export const commonRoute = (router: Router): void => {
    router.all('/healthcheck', healthCheck);
};
