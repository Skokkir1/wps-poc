import { basename } from 'path';
import * as requestPromise from 'request-promise';

class RestClient {
  protected TIME_OUT: number = 10000;
  protected connectionPool: boolean = true;

  public execute = async(restRequest: any) => {
    // console.log('restRequest', restRequest);
    const restOptions = this.buildOptions(restRequest);
    // console.log('restOptions', restOptions);
    const startTime = new Date();
    let endTime = new Date();
    try {
      let response = await requestPromise(restOptions);
      endTime = new Date();
      response.body = !!response.body ? response.body : {};
      //If response is a string, need to parse it to a JSON object
      if (!!response.body && this.checkForString(response.body)) {
        response.body = this.isParsable(response.body, response.headers["content-type"]) ?
            JSON.parse(response.body) : response.body;
      }
      // console.log('rest response', response.body);
      if (!!response.body && typeof response.body === "object" && response.statusCode < 500) {
        return response;
      } else {
        throw response;
      }
    } catch (err) {
      const errorResponse = this.buildInternalServerError(err);
      throw errorResponse;
    }
  };

  private buildOptions(request: any) {
    return {
      uri: request.url,
      method: request.method,
      headers: request.headers,
      qs: !!request.query ? request.query : undefined,
      body: !!request.body ? request.body : undefined,
      timeout: this.populateTimeout(request),
      json: !!request.json,
      resolveWithFullResponse: true,
      simple: false,
      forever: this.connectionPool,
    };
  }

  private isParsable = (body: string, contentType: string): boolean => {
    return body.includes("{") && contentType.startsWith("application/json");
  };

  private buildInternalServerError = (response: any) => ({
    body: {
      error: "internal_server_error",
      error_description:
        "Uh-oh, it looks like we have our wires crossed. Please try again later.",
    },
    statusCode: response.statusCode || 500,
  });

  private checkForString = (body: any) => !!body && typeof body === "string" && body.trim().length > 0;

  private populateTimeout = (restRequest: any) => {
    const { apiName } = restRequest;
    const apiNames: any = []; // api's which needs more timeout
    if (apiNames.includes(apiName)) {
      return 55000;
    } else {
      return this.TIME_OUT;
    }
  };
}

export const restClient = new RestClient();
