// NPM Module imports
import * as core from 'express-serve-static-core';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import { addResponseHeaders } from './middleware';

// Dev imports
import { applyRoutes } from './routes/applyRoutes';

const app: core.Express = express();
const router: core.Router = express.Router();

// mount json form parser
app.use(bodyParser.json());

// mount query string parser
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser());

app.all('*', addResponseHeaders);

// Adding the routes
app.use('/wps', applyRoutes(router));

/* It removes 'X-Powered-By: Express' header to avoid disclosure the app engine */
app.disable('x-powered-by');

export { app };
