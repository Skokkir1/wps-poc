import { Request, Response } from 'express';
import { simSearchRequestBuilder } from '../requestBuilder/simSearchRequestBuilder';
import { simSearchResponseBuilder } from '../responseBuilder/simSearchResponseBuilder';
import { simSearchService } from '../services/simSearch.service';

class SearchSimApi {
    public invoke = async (req: Request, res: Response) => {
        try {
            const simSearchRequest: any = simSearchRequestBuilder.build(req);
            const simSearchResponse: any = await simSearchService.invoke(simSearchRequest);
            const formattedResponse: any = simSearchResponseBuilder.build(simSearchResponse);
            res.status(formattedResponse.statusCode).send(formattedResponse.body);
        } catch (error) {
            throw error;
        }
    };
}

export const searchSimApi: SearchSimApi = new SearchSimApi();
