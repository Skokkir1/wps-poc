import { Request, Response } from 'express';
import { tokenRequestBuilder } from '../requestBuilder/tokenRequestBuilder';
import { tokenResponseBuilder } from '../responseBuilder/tokenResponseBuilder';
import { tokenService } from '../services/token.service';

class TokenApi {
  public invoke = async (req: Request, res: Response) => {
    try {
      const tokenRequest: any = tokenRequestBuilder.build(req);
      const tokenResponse: any = await tokenService.invoke(tokenRequest);
      const formattedResponse: any = tokenResponseBuilder.build(tokenResponse);
      res.status(formattedResponse.statusCode).send(formattedResponse.body);
    } catch (error) {
      throw error;
    }
  };
}

export const tokenApi: TokenApi = new TokenApi();
