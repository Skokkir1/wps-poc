
class SimSearchResponseBuilder {

    public build = (response: any) => {
        try {
            if (response) {
                // console.log('response builder', response);
                return response;
            } else {
                throw 'Invalid response from wms';
            }
        } catch (error) {
            throw error;
        }
    }
}

export const simSearchResponseBuilder:SimSearchResponseBuilder = new SimSearchResponseBuilder();
