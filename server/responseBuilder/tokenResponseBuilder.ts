import  { Response } from 'express';

class TokenResponseBuilder {

    public build = (response: any) => {
        try {
            if (response) {
                console.log('response builder', response.body);

                const formattedResponse = {
                    statusCode: response.statusCode || 200,
                    body: response.body
                };
                console.log('response builder', formattedResponse);
                return formattedResponse;
            } else {
                throw 'Invalid response from wms';
            }
        } catch (error) {
            throw error;
        }
    }
}

export const tokenResponseBuilder:TokenResponseBuilder = new TokenResponseBuilder();
