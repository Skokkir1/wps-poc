import { Request } from 'express';
import { restClient } from '../restClient/restClient';

class TokenService {
    public invoke = async(tokenRequest: Request) => {
        try {
            return await restClient.execute(tokenRequest);
        } catch (err) {
            throw err;
        }
    }
}

export const tokenService = new TokenService();
