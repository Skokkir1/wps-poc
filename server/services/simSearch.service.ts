import { restClient } from '../restClient/restClient';

class SimSearchService {
    public invoke = async(simSearchRequest: object) => {
        try {
            return await restClient.execute(simSearchRequest);
        } catch (err) {
            throw err;
        }
    }
}

export const simSearchService = new SimSearchService();
