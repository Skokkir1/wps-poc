import { cpus } from 'os';
import { fork, Worker, on as clusterOn } from 'cluster';
import { basename } from 'path';
import {configurator} from './config/configurator';
const envConfig = require(`./config/${configurator.getEnvironmentConfigFile()}`);
const fileName = basename(__filename);

/* Stores workers. * @type {Set} */
const wpsWorkers: Set<Worker> = new Set();

export const startWpsCluster = () => {
    startWorkers();
    process.on('uncaughtException', processExit('uncaughtException'));
    process.on('exit', processExit('exit'));
    clusterOn('exit', function(deadWorker) {
        deleteWorker(deadWorker);
        createWorker();
    });
};

const startWorkers = (): void => {
    const numberOfWorkers = cpus().length - envConfig.CLUSTER_COUNT;
    console.log(`cluster setting up workers.`, {fileName, args: numberOfWorkers});
    for (let i = 0; i < numberOfWorkers; i++) {
        createWorker();
    }
};

const createWorker = () => {
    const newWorker: Worker = fork();
    const {process:{ pid }} = newWorker;
    wpsWorkers.add(newWorker);
};

/** Kills the worker and deletes it from the {@link wpsWorkers}* */
 const deleteWorker = (deadWorker: Worker): void => {
    const { process: { pid } } = deadWorker;
    if (!deadWorker.isDead()) {
        console.log(`Killing worker ${pid}`, { fileName });
        deadWorker.kill();
    }
    console.log(`Deleting worker ${pid}`, { fileName });
    wpsWorkers.delete(deadWorker);
};


const processExit = (event: string) => {
    console.log(`Shutting down process due to ${event}`, {fileName} );
    return killWorkersProcess;
};

/** Kills all workers and exit process with code 1 * */
const killWorkersProcess = (reason: number | Error) => {
    const killingReason = (reason instanceof Error) ? reason.message : reason;
    if (wpsWorkers.size) {
        console.log(`Killing workers due to ${killingReason}`, {fileName} );
        wpsWorkers.forEach(deleteWorker);
    }
    process.exit(1);
};

