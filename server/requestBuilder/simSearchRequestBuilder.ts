import { Request } from 'express';
const commonConfig = require('../config/common-config.json');

class SimSearchRequestBuilder {
    public build = (req: Request) => {
        try {
            const wpsUrl = commonConfig.WPS_ENDPOINT + '/wholesale/v1/device/sim';
            const { body } = req;
            const { authorization } = req.headers;
            const tokenRequest = {
                url: wpsUrl,
                method: req.method,
                headers: {
                    authorization: authorization,
                    'sender-id': req.headers['sender-id'],
                    'partner-id': req.headers['partner-id'],
                    'partner-transaction-id': req.headers['partner-transaction-id']
                },
                body: body,
                json: true
            };
            return tokenRequest;
        } catch (error) {
            throw error;
        }
    };
}

export const simSearchRequestBuilder: SimSearchRequestBuilder = new SimSearchRequestBuilder();
