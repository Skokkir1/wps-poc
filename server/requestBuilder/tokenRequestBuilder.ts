import { Request } from 'express';
const commonConfig = require('../config/common-config.json');

class TokenRequestBuilder {
  public build = (req: Request) => {
    try {
      const wpsUrl = commonConfig.WPS_ENDPOINT + '/oauth2/v6/tokens';
      const { body } = req;
      const { authorization } = req.headers;
      const tokenRequest = {
        url: wpsUrl,
        method: req.method,
        headers: {
          authorization: authorization
        },
        body: body,
        json: true
      };
      return tokenRequest;
    } catch (error) {
      throw error;
    }
  };
}

export const tokenRequestBuilder: TokenRequestBuilder = new TokenRequestBuilder();
