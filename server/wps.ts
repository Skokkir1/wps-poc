import { basename } from 'path';
import { readFileSync } from 'fs';
import * as https from 'https';
import { app } from './app';
import { onError, onListening } from './utils/serverHandlers';
import { constants } from './constants/constants';
const commonConfig = require('./config/common-config.json');
import {configurator} from './config/configurator';
import {isMaster} from 'cluster';
import { startWpsCluster } from './wpsCluster';
const envConfig = require(`./config/${configurator.getEnvironmentConfigFile()}`);
const env = configurator.getEnvironment();
const KEYSTORE_PATH = commonConfig.KEYSTORE_PATH;
const fileName = basename(__filename);
const port = constants.PORT;

// https server options
const httpsOptions = {
    key: readFileSync(`${KEYSTORE_PATH}/${env}-server.key`),
    cert: readFileSync(`${KEYSTORE_PATH}/${env}-server.crt`)
};

const startWpsServer = () => {
    const server = https.createServer(httpsOptions, app);
    server.listen(port);
    server.on('error', onError(port));
    server.on('listening', onListening(server));
};

if (isMaster && envConfig.CLUSTER_SWITCH === 'ON') {
    startWpsCluster();
} else {
    startWpsServer();
}
