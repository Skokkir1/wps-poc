
export const getStandardErrorResponseObject = ( statusCode: number, error: string, error_description: string): any => {
    return {
        'status':{
            'code': statusCode,
            'error': [{
                'code': error,
                'description': error_description
            }]
        }
    };
};
