
import { NextFunction, Request, Response } from 'express';


export const addResponseHeaders = (req: Request, res: Response, next: NextFunction) => {
    const origin: any = req.headers.origin;
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Headers', 'Authorization,X-Authorization,');
    res.header('Access-Control-Expose-Headers', '');
    res.header('ORIG_SERVER', '');
    if (req.method === 'OPTIONS') {
        return res.end();
    } else {
        next();
    }
};
